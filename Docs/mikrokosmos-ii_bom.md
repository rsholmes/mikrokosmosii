# mikrokosmos-ii.kicad_sch BOM

Mon 26 Jun 2023 11:11:33 AM EDT

Generated from schematic by Eeschema 7.0.5-4d25ed1034~172~ubuntu22.04.1

**Component Count:** 39

| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| C1, C2 | 2 | 10uF | Electrolytic capacitor | Tayda | A-4349 |
| C3 | 1 | 1nF | Film capacitor | Tayda |  |
| C4, C5 | 2 | 100nF | Ceramic capacitor | Tayda |  |
| C6, C7 | 2 | 47nF | Film capacitor | Tayda |  |
| C8 | 1 | 330pF | Ceramic capacitor | Tayda |  |
| C9 | 1 | 2.2uF | Non polarized electrolytic capacitor | Tayda | A-4223 |
| D1–4 | 4 | 1N5817 | Schottky diode | Tayda | A-159 |
| J1 | 1 | AudioJack2_SwitchT | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) | Tayda | A-1121 |
| J2 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |
| J3, J5 | 2 | 2_pin_Molex_connector | KK254 Molex connector | Tayda | A-826 |
| J4, J6 | 2 | 2_pin_Molex_header | KK254 Molex header | Tayda | A-804 |
| J7 | 1 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J8 | 1 | DIP-8 | 8 pin DIP socket | Tayda | A-001 |
| LS1 | 1 | Piezo | Piezo element | Tayda | A-3001 |
| R1, R4 | 2 | 1M | Resistor | Tayda |  |
| R2 | 1 | 10M | Resistor | Tayda |  |
| R3, R8 | 2 | 1k | Resistor | Tayda |  |
| R5 | 1 | 2k | Resistor | Tayda |  |
| R6 | 1 | 100k | Resistor | Tayda |  |
| R7 | 1 | 4.7k | Resistor | Tayda |  |
| RV1 | 1 | A5k | Potentiometer | Tayda | A-3935 |
| U1 | 1 | TL072 | Dual op amp | Tayda | A-037 |
| ZKN1 | 1 | Knob_MF-A01 | Knob | Tayda | A-2828 |
| ZSC1–4 | 4 | Screw | screw | Tayda | A-1250 |
| ZSP1, ZSP2 | 2 | Spacer | spacer | Tayda | A-1232 |

## Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|2|R3, R8|
|100k|1|R6|
|1M|2|R1, R4|
|10M|1|R2|
|2k|1|R5|
|4.7k|1|R7|

